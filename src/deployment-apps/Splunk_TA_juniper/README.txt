Splunk Add-on for Juniper version 1.0.2
Copyright (C) 2005-2016 Splunk Inc. All Rights Reserved.

For documentation, see: http://docs.splunk.com/Documentation/AddOns/latest/Juniper
